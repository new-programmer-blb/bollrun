﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(new Vector3(0, 0, -1));
        float speed = 0.01f;  //移动速度
        int direct = boll_direct(this.transform.position.x, this.transform.position.y);
        bollrun(direct,speed);
    }
    //小球运动方向判断
    //-1:x+,0:x-,1:y+,2:Y-,3:停止
    int boll_direct(float x, float y)
    {
        //-1:x+,0:x-,1:y+,2:Y-,3:停止
        //z不变
        // x:-11 +11
        // y:-11 +11
        if (y == 5.5 && x >= -5.5 && x < 5.5)   //x+
        {
            return -1;
        }
        else if (y == -5.5 && x > -5.5 && x <= 5.5)
        {
            return 0;
        }
        else if (x == -5.5 && y >= -5.5 && y < 5.5)
        {
            return 1;
        }
        else if (x == 5.5 && y > -5.5 && y <= 5.5)
        {
            return 2;
        }
        return 3;
    }

    //小球运动
    void bollrun(int direct,float speed)
    {
        
        if(direct == -1)
        {
            run(5.5f, 5.5f, speed);
        }
        else if(direct == 0)
        {
            run(-5.5f, -5.5f, speed);
        }
        else if (direct == 1)
        {
            run(-5.5f, 5.5f, speed);
        }
        else if (direct == 2)
        {
            run(5.5f, -5.5f, speed);
        }
    }

    //游戏对象运动
    void run(float x, float y,float speed)
    {
        Vector3 vector3 = new Vector3(x,y,0);
        this.transform.position = Vector3.MoveTowards(transform.position, vector3, speed);
    }

}
